FROM python:3.10-slim
LABEL maintainer="MrFastwind@github.io"

#Copy App
WORKDIR /app
ADD . ./

#Install App
RUN pip install -r ./requirements.txt
ENTRYPOINT ["python"]
CMD ["./start.py"]

#Enviroment Variable
ENV TOKEN='BOT-TOKEN'
ENV DB_USER='USER'
ENV DB_PASSWORD='PASSWORD'
ENV DB_DATABASE='bot'
ENV DB_HOST='HOST'
ENV DB_PORT='PORT'
ENV LOG_LEVEL='DEBUG'