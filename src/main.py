import asyncio

import discord
from discord.ext import commands

from src.database import DatabaseMapperFactory as dmf

token = None


class Bot(commands.Bot):

    def __init__(self):
        # TODO: Add custom activity
        super().__init__(command_prefix=self.get_prefix, activity=discord.Game(">help"), intents=self.get_bot_intents())

        self.loop.create_task(self.load_cogs())

    async def get_prefix(self, message):
        guild_id = message.guild.id
        return dmf.get_guild(guild_id).get().prefix

    async def load_cogs(self):

        await self.wait_until_ready()
        await asyncio.sleep(1)

        self.load_extension('src.cogs.main')

        cogs = ['room', 'settings', 'permission']

        for cog in cogs:
            try:
                self.load_extension(f'src.cogs.commands.{cog}')
                print(f"{cog.capitalize()} cog loaded")
                # LOG: Loaded cog

            except Exception as e:
                print(e)

        print("EventHandler cog loaded")
        # LOG: Bot started

    async def on_message(self, message):
        # Ignore the event as it is processed by a cog although this needs to exist to avoid a double trigger
        pass

    @staticmethod
    def get_bot_intents():
        intents = discord.Intents.none()
        intents.guilds = True
        intents.members = True
        intents.voice_states = True
        intents.guild_messages = True

        return intents
