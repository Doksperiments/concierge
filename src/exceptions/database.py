class QueryExecutionAborted(Exception):

    def __init__(self, error):
        if error:
            self.error = error

    def __str__(self):
        return "The query execution was aborted for this reason: {}".format(self.error)


class DuplicateItemError(Exception):

    def __init__(self):
        pass

    def __str__(self):
        return "The query result was already in the database, try using a get function"


class WrongIdFormat(Exception):

    def __init__(self):
        pass

    def __str__(self):
        return "The id provided wasn't an 18 digits long integer"


class ValueNotInRange(Exception):

    def __init__(self, value_name, value_range):
        self.value_name = value_name
        self.value_range = value_range

    def __str__(self):
        return f"{self.value_name} not in required range ({self.value_range})"


class PrefixLengthLimit(Exception):

    def __init__(self):
        pass

    def __str__(self):
        return "The maximum length for the prefix is 3"


class LanguageNotSupported(Exception):

    def __init__(self, lang):
        self.lang = lang

    def __str__(self):
        return f"The specified language ({self.lang}) isn't supported yet"


class ValueTooLong(Exception):

    def __init__(self, value_name, value_max):
        self.value_name = value_name
        self.value_max = value_max

    def __str__(self):
        return f"The value for {self.value_name} inserted was too long (MAX {self.value_max})"


class UserAlreadyInvited(Exception):

    def __init__(self):
        pass

    def __str__(self):
        return "The user has already been invited to the room"


class InvitedOwnerError(Exception):

    def __init__(self):
        pass

    def __str__(self):
        return "Can't invite room's owner to his room"


class KickedOwnerError(Exception):

    def __init__(self):
        pass

    def __str__(self):
        return "Can't kick room's owner from his room"
