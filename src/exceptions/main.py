class VariableNotFoundError(Exception):

    def __init__(self, message):
        if message:
            self.message = message

    def __str__(self):
        return "Couldn't find {} variable in .env file".format(self.message)
