import json
import logging

from discord import Embed

from .embedcompiler import EmbedCompiler
from .embedcompiler.src.objects.formatter import EmbedFormatter


class LanguageLoader:

    @staticmethod
    def __load_json(lang) -> dict:
        # Does the file remain cached? Or does it get closed after returning?
        return json.load(open(f'src/resource/lang/{lang}.json', 'r', encoding="utf-8"))

    @staticmethod
    def get_message(name, lang, **fmt_str) -> str:

        try:
            msg = LanguageLoader.__load_json(lang)['messages'][name]
        except KeyError:
            logging.warning(f"The message '{name}' in the language '{lang}' couldn't be found, used default instead")
            msg = LanguageLoader.__load_json("en")['messages'][name]

        return EmbedFormatter().format(msg, **fmt_str) if fmt_str else msg

    @staticmethod
    def get_embed(name, lang, **fmt_str) -> Embed:

        try:
            embed = LanguageLoader.__load_json(lang)['embeds'][name]
        except KeyError:
            logging.warning(f"The embed '{name}' in the language '{lang}' couldn't be found, used default instead")
            embed = LanguageLoader.__load_json("en")['embeds'][name]

        return Embed.from_dict(EmbedCompiler().from_dict(embed, fmt_str=fmt_str).to_dict())
