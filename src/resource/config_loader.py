import os
from pathlib import Path

from dotenv import load_dotenv

from src.exceptions.main import VariableNotFoundError


class ConfigLoader:

    env_path: Path = Path('.env')
    __conn_args: dict = {}
    __log_level: str = None
    __token: str = None
    __default: dict = {
        'user': 'root',
        'host': '127.0.0.1',
        'port': '3306',
        'password': ''
    }

    def __init__(self):
        ConfigLoader.load_env()

    @classmethod
    def load_env(cls):
        load_dotenv(cls.env_path)

        for item in ['user', 'password', 'database', 'host', 'port']:
            value = os.getenv(f"DB_{item.upper()}")
            cls.__conn_args[item] = value if value else cls.__default[item]

        cls.__log_level = os.getenv('LOG_LEVEL')
        cls.__token = os.getenv('TOKEN')

        if not cls.__token:
            # LOG: CRITICAL: TOKEN NOT FOUND
            raise VariableNotFoundError('BOT TOKEN')

        if not cls.__log_level:
            cls.__log_level = 'WARNING'

    @classmethod
    def get_arg(cls, name: str):

        if None in cls.__conn_args.values():
            cls.load_env()

        if name == 'log_level':
            return cls.__log_level

        arg = cls.__conn_args[name]
        if not arg and name != 'password':
            raise VariableNotFoundError(f'DB_{name.upper()}')

        return arg

    @classmethod
    def get_token(cls):

        if not cls.__conn_args:
            cls.load_env()

        return cls.__token
