from abc import ABC, abstractmethod


class LoggerInterface(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def write(self, level, message):
        raise NotImplementedError

    @abstractmethod
    def get_log_level(self):
        raise NotImplementedError

    @abstractmethod
    def load_log_level(self):
        raise NotImplementedError

    @abstractmethod
    def add_file_handler(self, file):
        raise NotImplementedError
