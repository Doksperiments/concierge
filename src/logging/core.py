import logging
from pathlib import Path

from logging.abstracts.core import LoggerInterface

env_path = Path('./venv') / '.env'


class Logger(LoggerInterface, logging):

    def __init__(self):

        self.logger = logging.basicConfig(
            filename=f"{__name__}.log",
            filemode="w",
            level=self.get_log_level()
            format="%(asctime)s : %(module)s : %(levelname)s"
        )
        self._log_level = None

    def write(self, level, message):
        pass

    def get_log_level(self):

        if not self._log_level:
            self.load_log_level()

        return self._log_level

    def load_log_level(self):

        load_env(env_path)

        levels = {
            "CRITICAL": logging.CRITICAL,
            "FATAL": logging.CRITICAL,
            "ERROR": logging.ERROR,
            "WARNING": logging.WARNING,
            "WARN": logging.WARNING,
            "INFO": logging.INFO,
            "DEBUG": logging.DEBUG
        }

        # rewrite using levels.get()

        self._log_level = levels[str(os.getenv('LOG_LEVEL'))]

    def add_file_handler(self, name):

        f_handler = logging.FileHandler(
            filename=f'{name}.log',
            encoding='utf-8',
            mode='w'
            )

        self.logger.addHandler(f_handler)
