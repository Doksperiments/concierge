from abstracts.logger import DatabaseLoggerInterface


class DatabaseLogger(DatabaseLoggerInterface):

    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(DatabaseLogger, cls).__new__(cls)
            # Init stuff
        return cls._instance

    def write(self, level, message):
        print(f"{level}: {message}")

    def get_log_level(self):
        pass

    def load_log_level(self):
        pass

    def add_file_handler(self, name):
        pass
