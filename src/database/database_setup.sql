/*Don't Use*/
CREATE DATABASE bot;

CREATE TABLE bot.guilds (
    guild_id BIGINT(20) UNSIGNED UNIQUE NOT NULL,
    category_id BIGINT(20) UNSIGNED,                              /*categoria nella quale creare i canali*/
    log_channel_id BIGINT(20) UNSIGNED,                            /*canale nel quale far apparire l'output di tutti i comandi istanziati dal bot*/
    prefix VARCHAR(3) NOT NULL DEFAULT '>',             /*prefisso per richiamare il bot*/
    lobby_id BIGINT(20) UNSIGNED,                       /*id del canale per la creazione automatica delle stanze*/
    expire_time INT(2) DEFAULT 30,                         /*canale nel quale entrare per creare una stanza personale (NULL disattivo - 60 secondi max)*/
    lang VARCHAR(5) DEFAULT 'it',                     /*codice della lingua utilizzata nei messaggi del bot sul server*/

    /*Chiavi*/
    PRIMARY KEY (guild_id)
);

CREATE TABLE bot.users (
    user_id BIGINT(20) UNSIGNED UNIQUE NOT NULL,
    plus BOOLEAN NOT NULL DEFAULT 0,

    /*Chiavi*/
    PRIMARY KEY (user_id)
);

CREATE TABLE bot.roomconfig (
    config_id BIGINT(18) UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT,
    user_fk BIGINT(20) UNSIGNED,
    guild_fk BIGINT(20) UNSIGNED,
    name VARCHAR(100) NOT NULL DEFAULT "|{username}| room",                                 /*nome da applicare alla stanza*/
    userlimit INT(2) UNSIGNED NOT NULL DEFAULT 0,                        /*massimo numero di utenti permessi in una stanza(0-99)*/
    bitrate INT(5) UNSIGNED NOT NULL DEFAULT 64000,                           /*bitrate del canale vocale(8000-96000)*/
    topic VARCHAR(1024) DEFAULT "Use {prefix} room set to customize it",                                        /*argomento del canale testuale(0-1024)*/

    /*Chiavi*/
    PRIMARY KEY (config_id),
    FOREIGN KEY (user_fk) REFERENCES users (user_id) ON DELETE CASCADE,
    FOREIGN KEY (guild_fk) REFERENCES guilds (guild_id) ON DELETE CASCADE
);

CREATE TABLE bot.rooms (
    room_id BIGINT(18) UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT,
    guild_fk BIGINT(20) UNSIGNED NOT NULL,
    user_fk BIGINT(20) UNSIGNED,
    text_id BIGINT(20) UNSIGNED,                                                           /*id del canale testuale*/
    voice_id BIGINT(20) UNSIGNED,                                                          /*id del canale vocale*/
    creation_date DATETIME DEFAULT CURRENT_TIMESTAMP,                                      /*data di creazione della stanza*/

    /*Chiavi*/
    PRIMARY KEY (room_id),
    FOREIGN KEY (guild_fk) REFERENCES guilds (guild_id) ON DELETE CASCADE,
    FOREIGN KEY (user_fk) REFERENCES users (user_id) ON DELETE CASCADE
);

CREATE TABLE bot.roomusers (
    bond_id INT UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT,
    room_fk BIGINT(18) UNSIGNED NOT NULL,
    user_fk BIGINT(20) UNSIGNED NOT NULL,

    /*Chiavi*/
    PRIMARY KEY (bond_id),
    FOREIGN KEY (room_fk) REFERENCES rooms (room_id) ON DELETE CASCADE,
    FOREIGN KEY (user_fk) REFERENCES users (user_id) ON DELETE CASCADE,
    CONSTRAINT RoomUsers_UC UNIQUE (room_fk, user_fk)
);

CREATE TABLE bot.roles (
    role_id BIGINT(20) UNSIGNED NOT NULL,
    guild_fk BIGINT(20) UNSIGNED NOT NULL,

    /*Chiavi*/
    PRIMARY KEY (role_id),
    FOREIGN KEY (guild_fk) REFERENCES guilds (guild_id) ON DELETE CASCADE
);

CREATE TABLE bot.permissions (
    permission_id BIGINT(18) UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(100),

    /*Chiavi*/
    PRIMARY KEY (permission_id)
);

CREATE TABLE bot.rolepermissions (
    bond_id INT UNSIGNED UNIQUE NOT NULL AUTO_INCREMENT,
    role_fk BIGINT(20) UNSIGNED NOT NULL,
    permission_fk BIGINT(18) UNSIGNED NOT NULL,

    /*Chiavi*/
    PRIMARY KEY (bond_id),
    FOREIGN KEY (role_fk) REFERENCES roles (role_id) ON DELETE CASCADE,
    FOREIGN KEY (permission_fk) REFERENCES permissions (permission_id) ON DELETE CASCADE,
    CONSTRAINT RolePermissions_UC UNIQUE (role_fk, permission_fk)
)
