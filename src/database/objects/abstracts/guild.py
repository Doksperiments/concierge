from abc import ABC, abstractmethod


class GuildInterface(ABC):

    @abstractmethod
    def __init__(self, connector, guild_id):

        self.id = guild_id

        self.__connector = connector
        self.__table = self.__connector['guilds']

        self.__log_channel = None
        self.__lobby = None
        self.__category = None
        self.__expire_time = 30
        self.__prefix = '>'
        self.__lang = 'en'

    @abstractmethod
    def add(self):
        raise NotImplementedError

    @abstractmethod
    def remove(self):
        raise NotImplementedError

    @abstractmethod
    def exists(self):
        raise NotImplementedError

    @abstractmethod
    def get(self):
        raise NotImplementedError

    @abstractmethod
    def get_all(self):
        raise NotImplementedError

    @abstractmethod
    def get_roles(self):
        raise NotImplementedError

    # Properties

    @property
    @abstractmethod
    def log_channel(self):
        raise NotImplementedError

    @log_channel.setter
    @abstractmethod
    def log_channel(self, log_channel_id):
        raise NotImplementedError

    @property
    @abstractmethod
    def lobby(self):
        raise NotImplementedError

    @lobby.setter
    @abstractmethod
    def lobby(self, lobby_id):
        raise NotImplementedError

    @property
    @abstractmethod
    def category(self):
        raise NotImplementedError

    @category.setter
    @abstractmethod
    def category(self, category_id):
        raise NotImplementedError

    @property
    @abstractmethod
    def expire_time(self):
        raise NotImplementedError

    @expire_time.setter
    @abstractmethod
    def expire_time(self, expire_time):
        raise NotImplementedError

    @property
    @abstractmethod
    def prefix(self):
        raise NotImplementedError

    @prefix.setter
    @abstractmethod
    def prefix(self, prefix):
        raise NotImplementedError

    @property
    @abstractmethod
    def lang(self):
        raise NotImplementedError

    @lang.setter
    @abstractmethod
    def lang(self, lang):
        raise NotImplementedError
