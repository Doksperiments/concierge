from abc import ABC, abstractmethod


class PermissionInterface(ABC):

    @abstractmethod
    def __init__(self, connector, name):

        self.name = name

        self.__connector = connector
        self.__table = self.__connector['permissions']

        self.__id = None
        self.__description = None

    @abstractmethod
    def add(self):
        raise NotImplementedError

    @abstractmethod
    def remove(self):
        raise NotImplementedError

    @abstractmethod
    def exists(self):
        raise NotImplementedError

    @abstractmethod
    def get(self):
        raise NotImplementedError

    @abstractmethod
    def get_all(self):
        raise NotImplementedError

    # Properties

    @property
    @abstractmethod
    def description(self):
        raise NotImplementedError

    @description.setter
    @abstractmethod
    def description(self, description):
        raise NotImplementedError

    @property
    @abstractmethod
    def id(self):
        raise NotImplementedError

    @id.setter
    @abstractmethod
    def id(self, id):
        raise NotImplementedError
