from abc import ABC, abstractmethod


class UserInterface(ABC):

    @abstractmethod
    def __init__(self, connector, user_id):
        self.id = user_id

        self.__connector = connector
        self.__table = self.__connector['users']

        self.__plus = False

    @abstractmethod
    def add(self):
        raise NotImplementedError

    @abstractmethod
    def remove(self):
        raise NotImplementedError

    @abstractmethod
    def exists(self):
        raise NotImplementedError

    @abstractmethod
    def get(self):
        raise NotImplementedError

    @abstractmethod
    def get_all(self):
        raise NotImplementedError

    # Properties

    @property
    @abstractmethod
    def plus(self):
        raise NotImplementedError

    @plus.setter
    @abstractmethod
    def plus(self, state):
        raise NotImplementedError
