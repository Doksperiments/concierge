from abc import ABC, abstractmethod


class RoomInterface(ABC):

    @abstractmethod
    def __init__(self, connector, owner_id, guild_id):

        self.owner = None
        self.guild = None

        self.__connector = connector
        self.__table = self.__connector['rooms']

        self.__voice = None
        self.__text = None
        self.__id = None
        self.__invited_users = []

    @abstractmethod
    def add(self):
        raise NotImplementedError

    @abstractmethod
    def remove(self):
        raise NotImplementedError

    @abstractmethod
    def invite_user(self, user):
        raise NotImplementedError

    @abstractmethod
    def kick_user(self, user):
        raise NotImplementedError

    @abstractmethod
    def exists(self):
        raise NotImplementedError

    @abstractmethod
    def get(self):
        raise NotImplementedError

    @abstractmethod
    def get_all(self, guild_id):
        raise NotImplementedError

    # Properties

    @property
    @abstractmethod
    def voice(self):
        raise NotImplementedError

    @voice.setter
    @abstractmethod
    def voice(self, voice_id):
        raise NotImplementedError

    @property
    @abstractmethod
    def text(self):
        raise NotImplementedError

    @text.setter
    @abstractmethod
    def text(self, text_id):
        raise NotImplementedError
