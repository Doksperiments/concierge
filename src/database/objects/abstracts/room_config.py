from abc import ABC, abstractmethod


class RoomConfigInterface(ABC):

    @abstractmethod
    def __init__(self, connector, user_id=None, guild_id=None):

        self.user = None
        self.guild = None

        self.__connector = connector
        self.__table = self.__connector['roomconfig']

        self.__name = None
        self.__topic = None
        self.__bitrate = None
        self.__userlimit = None

    @abstractmethod
    def add(self):
        raise NotImplementedError

    @abstractmethod
    def remove(self):
        raise NotImplementedError

    @abstractmethod
    def exists(self):
        raise NotImplementedError

    @abstractmethod
    def get(self):
        raise NotImplementedError

    # PROPERTIES

    @property
    @abstractmethod
    def name(self):
        raise NotImplementedError

    @name.setter
    @abstractmethod
    def name(self, name):
        raise NotImplementedError

    @property
    @abstractmethod
    def topic(self):
        raise NotImplementedError

    @topic.setter
    @abstractmethod
    def topic(self, topic):
        raise NotImplementedError

    @property
    @abstractmethod
    def bitrate(self):
        raise NotImplementedError

    @bitrate.setter
    @abstractmethod
    def bitrate(self, bitrate):
        raise NotImplementedError

    @property
    @abstractmethod
    def userlimit(self):
        raise NotImplementedError

    @userlimit.setter
    @abstractmethod
    def userlimit(self, userlimit):
        raise NotImplementedError
