from abc import ABC, abstractmethod


class RoleInterface(ABC):

    @abstractmethod
    def __init__(self, connector, role_id, guild_id):

        self.id = role_id
        self.guild = None

        self.__connector = connector
        self.__table = self.__connector['roles']

        self.__permissions = None

    @abstractmethod
    def add(self):
        raise NotImplementedError

    @abstractmethod
    def remove(self):
        raise NotImplementedError

    @abstractmethod
    def add_permission(self, permission):
        raise NotImplementedError

    @abstractmethod
    def remove_permission(self, permission):
        raise NotImplementedError

    @abstractmethod
    def remove_all_permissions(self):
        raise NotImplementedError

    @abstractmethod
    def has_permission(self, permission):
        raise NotImplementedError

    @abstractmethod
    def exists(self):
        raise NotImplementedError

    @abstractmethod
    def get(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def permissions(self):
        return self.__permissions

    @abstractmethod
    def get_all(self):
        raise NotImplementedError
