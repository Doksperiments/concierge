import re

from src.exceptions.database import *
from .abstracts.room import RoomInterface


class Room(RoomInterface):

    def __init__(self, connector, owner_id, guild_id):

        self.owner_id = owner_id
        self.guild_id = guild_id

        self.__connector = connector
        self.__table = self.__connector['rooms']

        self.__voice = None
        self.__text = None
        self.__id = None
        self.__invited_users = []

        self.id = property(self.__id)

    def add(self):

        if self.exists() is False:
            self.__table.insert(dict(user_fk=self.owner_id, guild_fk=self.guild_id))

        else:
            raise DuplicateItemError

        return self

    def remove(self):
        self.__table.delete(user_fk=self.owner_id, guild_fk=self.guild_id)

    def invite_user(self, user):

        if user.id == self.owner_id:
            raise InvitedOwnerError

        if user.id not in self.__invited_users:

            self.__connector['roomusers'].insert(dict(user_fk=user.id, room_fk=self.__id))

            self.__invited_users.append(user.id)

            return self

        else:
            raise UserAlreadyInvited

    def kick_user(self, user):

        if user.id == self.owner_id:
            raise KickedOwnerError

        if user.id in self.__invited_users:

            self.__connector['roomusers'].delete(user_fk=user.id, room_fk=self.__id)

            self.__invited_users.remove(user.id)

        return self

    def exists(self):

        poe = self.__table.find_one(user_fk=self.owner_id)

        if poe is None:
            return False

        if poe['user_fk'] is not None:
            return True

        return False

    def get(self):

        if not self.exists():
            return None

        options = self.__table.find_one(user_fk=self.owner_id, guild_fk=self.guild_id)

        if not options:
            return self

        self.__voice = options["voice_id"]
        self.__text = options["text_id"]
        self.__id = options["room_id"]

        users_list = self.__connector['roomusers'].find(room_fk=self.__id)

        self.__invited_users = []

        for user in users_list:
            self.__invited_users.append(user['user_fk'])

        return self

    def get_all(self, guild_id):

        rooms = []
        raw_rooms = self.__table.find(guild_fk=guild_id)

        for room in raw_rooms:
            rooms.append(
                Room(self.__connector, room['user_fk'], guild_id).get()
            )

        return rooms

    # Properties

    @property
    def voice(self):
        return self.__voice

    @voice.setter
    def voice(self, voice_id):

        if re.fullmatch(r"[0-9]{17,20}", str(voice_id)):
            self.__voice = voice_id
            self.__table.upsert(
                dict(user_fk=self.owner_id, guild_fk=self.guild_id, voice_id=self.__voice), ['user_fk', 'guild_fk'])

        else:
            raise WrongIdFormat

    @property
    def text(self):
        return self.__text

    @text.setter
    def text(self, text_id):

        if re.fullmatch(r"[0-9]{17,20}", str(text_id)):
            self.__text = text_id
            self.__table.upsert(
                dict(user_fk=self.owner_id, guild_fk=self.guild_id, text_id=self.__text), ['user_fk', 'guild_fk'])

        else:
            raise WrongIdFormat
