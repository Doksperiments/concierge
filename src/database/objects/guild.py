import re

from src.exceptions.database import *
from .abstracts.guild import GuildInterface
from .role import Role


class Guild(GuildInterface):

    def __init__(self, connector, guild_id):

        self.id = guild_id

        self.__connector = connector
        self.__table = self.__connector['guilds']

        self.__log_channel = None
        self.__lobby = None
        self.__category = None
        self.__expire_time = 30
        self.__prefix = '>'
        self.__lang = 'en'

    def add(self):

        if self.exists() is False:

            self.__table.insert(dict(guild_id=self.id))
            return self

        else:
            raise DuplicateItemError

    def remove(self):
        self.__table.delete(guild_id=self.id)

    def exists(self):

        poe = self.__table.find_one(guild_id=self.id)

        if poe is None:
            return False

        if poe['guild_id'] is not None:
            return True

        return False

    def get(self):

        if not self.exists():
            return None

        options = self.__table.find_one(guild_id=self.id)

        if not options:
            return self

        del options["guild_id"]

        self.__log_channel = options.get('log_channel_id', self.__log_channel)
        self.__lobby = options.get('lobby_id', self.__lobby)
        self.__category = options.get('category_id', self.__category)
        self.__expire_time = options.get('expire_time', self.__expire_time)
        self.__prefix = options.get('prefix', self.__prefix)
        self.__lang = options.get('lang', self.__lang)

        return self

    def get_all(self):

        guilds = []
        raw_guilds = self.__table.find()

        for guild in raw_guilds:
            guilds.append(
                Guild(self.__connector, guild['guild_id']).get()
            )

        return guilds

    def get_roles(self):

        roles = []

        if not self.exists():
            return None

        roles_list = self.__connector['roles'].find(guild_fk=self.id)

        for role in roles_list:
            roles.append(Role(self.__connector, role['role_id'], self.id))

        return roles

    @property
    def log_channel(self):
        return self.__log_channel

    @log_channel.setter
    def log_channel(self, log_channel_id):

        if re.fullmatch(r"[0-9]{17,20}", str(log_channel_id)) or log_channel_id == 0:
            if log_channel_id == 0:
                self.__log_channel = None
            else:
                self.__log_channel = log_channel_id

            self.__table.upsert(dict(guild_id=self.id, log_channel_id=self.__log_channel), ['guild_id'])
        else:
            raise WrongIdFormat

    @property
    def lobby(self):
        return self.__lobby

    @lobby.setter
    def lobby(self, lobby_id):

        if re.fullmatch(r"[0-9]{17,20}", str(lobby_id)) or lobby_id == 0:
            if lobby_id == 0:
                self.__lobby = None
            else:
                self.__lobby = lobby_id

            self.__table.upsert(dict(guild_id=self.id, lobby_id=self.__lobby), ['guild_id'])
        else:
            raise WrongIdFormat

    @property
    def category(self):
        return self.__category

    @category.setter
    def category(self, category_id):

        if re.fullmatch(r"[0-9]{17,20}", str(category_id)) or category_id == 0:
            if category_id == 0:
                self.__category = None
            else:
                self.__category = category_id

            self.__table.upsert(dict(guild_id=self.id, category_id=self.__category), ['guild_id'])
        else:
            raise WrongIdFormat

    @property
    def expire_time(self):
        return self.__expire_time

    @expire_time.setter
    def expire_time(self, expire_time):

        if expire_time in range(0, 60):
            if expire_time == 0:
                self.__expire_time = None
            else:
                self.__expire_time = expire_time

            self.__table.upsert(dict(guild_id=self.id, expire_time=self.__expire_time), ['guild_id'])

        else:
            raise ValueNotInRange('expire_time', '0-60')

    @property
    def prefix(self):
        return self.__prefix

    @prefix.setter
    def prefix(self, prefix):

        if len(prefix) < 4:
            self.__prefix = str(prefix)
            self.__table.upsert(dict(guild_id=self.id, prefix=self.__prefix), ['guild_id'])

        else:
            raise PrefixLengthLimit

    @property
    def lang(self):
        return self.__lang

    @lang.setter
    def lang(self, lang):

        supported_lang = ['en', 'it']
        if lang in supported_lang:
            self.__lang = lang
            self.__table.upsert(dict(guild_id=self.id, lang=self.__lang), ['guild_id'])

        else:
            raise LanguageNotSupported(lang)
