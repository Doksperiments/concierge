from src.exceptions.database import DuplicateItemError
from .abstracts.user import UserInterface


class User(UserInterface):

    def __init__(self, connector, user_id):
        self.id = user_id

        self.__connector = connector
        self.__table = self.__connector['users']

        self.__plus = False

    def add(self):

        if self.exists() is False:

            self.__table.insert(dict(user_id=self.id))
            return self

        else:
            raise DuplicateItemError

    def remove(self):
        self.__table.delete(user_id=self.id)

    def exists(self):

        poe = self.__table.find_one(user_id=self.id)

        if poe is None:
            return False

        if poe['user_id'] is not None:
            return True

        return False

    def get(self):

        if not self.exists():
            return None

        options = self.__table.find_one(user_id=self.id)

        if not options:
            return self

        self.__plus = options["plus"]

        return self

    def get_all(self):
        return self.__table.find()

    # Properties

    @property
    def plus(self):
        return self.__plus

    @plus.setter
    def plus(self, state):
        if type(state) == bool:
            self.__plus = state
            self.__table.upsert(dict(user_id=self.id, plus=self.__plus), ['user_id'])
