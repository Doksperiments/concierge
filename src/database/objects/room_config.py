from src.exceptions.database import *
from .abstracts.room_config import RoomConfigInterface


class RoomConfig(RoomConfigInterface):

    def __init__(self, connector, user_id=None, guild_id=None):

        self.user_id = user_id
        self.guild_id = guild_id

        self.__connector = connector
        self.__table = self.__connector['roomconfig']

        self.__name = None
        self.__topic = None
        self.__bitrate = None
        self.__userlimit = None

    def add(self):

        if self.exists() is False:
            self.__table.insert(dict(user_fk=self.user_id, guild_fk=self.guild_id))

        else:
            raise DuplicateItemError

        return self

    def remove(self):
        self.__table.delete(user_fk=self.user_id, guild_fk=self.guild_id)

    def exists(self):

        poe = self.__table.find_one(user_fk=self.user_id, guild_fk=self.guild_id)

        if poe is None:
            return False

        if poe['user_fk'] is not None or poe['guild_fk'] is not None:
            return True

        return False

    def get(self):

        if not self.exists():
            return None

        options = self.__table.find_one(user_fk=self.user_id, guild_fk=self.guild_id)

        if not options:
            return self

        self.__name = options['name']
        self.__topic = options["topic"]
        self.__bitrate = options["bitrate"]
        self.__userlimit = options["userlimit"]

        return self

    # PROPERTIES

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, name):

        if len(name) in range(2, 101):
            self.__name = name
            self.__table.upsert(
                dict(user_fk=self.user_id, guild_fk=self.guild_id, name=self.__name), ['user_fk', 'guild_fk'])

    @property
    def topic(self):
        return self.__topic

    @topic.setter
    def topic(self, topic):
        if len(topic) in range(1, 1025):
            self.__topic = topic
            self.__table.upsert(
                dict(user_fk=self.user_id, guild_fk=self.guild_id, topic=self.__topic), ['user_fk', 'guild_fk'])

    @property
    def bitrate(self):
        return self.__bitrate

    @bitrate.setter
    def bitrate(self, bitrate):

        if bitrate in range(8000, 96001):
            self.__bitrate = bitrate
            self.__table.upsert(
                dict(user_fk=self.user_id, guild_fk=self.guild_id, bitrate=self.__bitrate), ['user_fk', 'guild_fk'])

    @property
    def userlimit(self):
        return self.__userlimit

    @userlimit.setter
    def userlimit(self, userlimit):
        if int(userlimit) in range(100):
            self.__userlimit = userlimit
            self.__table.upsert(
                dict(user_fk=self.user_id, guild_fk=self.guild_id, userlimit=self.__userlimit), ['user_fk', 'guild_fk'])
