from src.exceptions.database import DuplicateItemError, ValueTooLong
from .abstracts.permission import PermissionInterface


class Permission(PermissionInterface):

    def __init__(self, connector, name):

        self.name = name

        self.__connector = connector
        self.__table = self.__connector['permissions']

        self.__id = None
        self.__description = None

    def add(self):

        if self.exists() is False:

            self.__table.insert(dict(name=self.name))

            return self

        else:
            raise DuplicateItemError

    def remove(self):
        self.__table.delete(name=self.name)

    def exists(self):

        poe = self.__table.find_one(name=self.name)

        if poe is None:
            return False

        if poe['name'] is not None:
            return True

        return False

    def get(self):

        if not self.exists():
            return None

        options = self.__table.find_one(name=self.name)

        if not options:
            return self

        self.__description = options["description"]
        self.__id = options["permission_id"]

        return self

    def get_all(self):
        return self.__table.find()

    # Properties

    @property
    def description(self):
        return self.__description

    @description.setter
    def description(self, description):

        if len(description) < 101:
            self.__description = str(description)
            self.__table.upsert(dict(name=self.name, description=self.__description), ['name'])

        else:
            raise ValueTooLong('description', '100')

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        pass
