from src.exceptions.database import DuplicateItemError
from .abstracts.role import RoleInterface
from .permission import Permission


class Role(RoleInterface):

    def __init__(self, connector, role_id, guild_id):

        self.id = role_id
        self.guild_id = guild_id

        self.__connector = connector
        self.__table = self.__connector['roles']

        self.__permissions = []

    def add(self):

        if self.exists() is False:
            self.__table.insert(dict(role_id=self.id, guild_fk=self.guild_id))

            return self

        else:
            raise DuplicateItemError

    def remove(self):

        self.remove_all_permissions()

        self.__table.delete(role_id=self.id)

    def add_permission(self, permission):

        if self.has_permission(permission.name):
            raise DuplicateItemError

        self.__connector['rolepermissions'].insert(dict(role_fk=self.id, permission_fk=permission.get().id))

        self.__permissions.append(permission)

        return self

    def remove_permission(self, permission):

        self.__connector['rolepermissions'].delete(role_fk=self.id, permission_fk=permission.get().id)

        self.get()

        for perm in self.__permissions:
            if perm.name == permission.name:
                self.__permissions.remove(perm)

        return self

    def remove_all_permissions(self):

        self.__connector['rolepermissions'].delete(role_fk=self.id)

        self.__permissions = []

        return self

    def has_permission(self, permission_name):

        permission = Permission(self.__connector, permission_name).get()

        poe = self.__connector['rolepermissions'].find_one(role_fk=self.id, permission_fk=permission.id)

        if not poe:
            return False

        if poe["role_fk"]:
            return True

        return False

    def exists(self):

        poe = self.__table.find_one(role_id=self.id)

        if poe is None:
            return False

        if poe['role_id'] is not None:
            return True

        return False

    def get(self):

        if not self.exists():
            return None

        query = f"""
            SELECT name FROM Permissions
            INNER JOIN RolePermissions
            ON permission_id = permission_fk
            WHERE role_fk={self.id}
            """

        permissions = self.__connector.query(query)

        self.__permissions = []

        if not permissions:
            return self

        for permission in permissions:
            self.__permissions.append(Permission(self.__connector, permission["name"]).get())

        return self

    @property
    def permissions(self):
        return self.__permissions

    def get_all(self):
        return self.__table.find()
