import dataset

from src.database import User, Guild, Role, Room, RoomConfig, Permission
from src.resource import ConfigLoader


class MySQLConnectorBuilder:

    def __init__(self):

        self.__credentials = None
        self.__server = None
        self.__database = None

    def set_credentials(self):
        self.__credentials = tuple(ConfigLoader.get_arg(x) for x in ['user', 'password'])

    def set_server(self):
        self.__server = tuple(ConfigLoader.get_arg(x) for x in ['host', 'port'])

    def set_database(self):
        self.__database = ConfigLoader.get_arg('database')

    def get_result(self):
        return dataset.connect(
            "mysql+mysqlconnector://{user}:{password}@{host}:{port}/{dbname}".format(
                user=self.__credentials[0],
                password=self.__credentials[1],
                host=self.__server[0],
                port=self.__server[1],
                dbname=self.__database
            ),
            engine_kwargs={'pool_recycle': 3600}
        )


class DatabaseMapperFactory:

    @classmethod
    def get_guild(cls, guild_id=None):
        connector = cls.__get_connector()
        return Guild(connector, guild_id)

    @classmethod
    def get_permission(cls, permission_name=None):
        connector = cls.__get_connector()
        return Permission(connector, permission_name)

    @classmethod
    def get_role(cls, role_id=None, guild_id=None):
        connector = cls.__get_connector()
        return Role(connector, role_id, guild_id)

    @classmethod
    def get_room(cls, user_id=None, guild_id=None):
        connector = cls.__get_connector()
        return Room(connector, user_id, guild_id)

    @classmethod
    def get_config(cls, user_id=None, guild_id=None):
        connector = cls.__get_connector()

        if user_id:
            return RoomConfig(connector, user_id=user_id)
        return RoomConfig(connector, guild_id=guild_id)

    @classmethod
    def get_user(cls, user_id=None):
        connector = cls.__get_connector()
        return User(connector, user_id)

    @classmethod
    def __get_connector(cls):

        builder = MySQLConnectorBuilder()

        builder.set_credentials()
        builder.set_server()
        builder.set_database()

        return builder.get_result()
