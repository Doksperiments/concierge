import discord
from discord.ext import commands

from src.database import DatabaseMapperFactory as dmf


async def has_permission(permission_name):
    async def predicate(ctx):
        for role in ctx.author.roles:

            database_role = dmf.get_role(role.id, ctx.guild.id)
            if database_role.exists():
                if database_role.has_permission(permission_name):
                    return True

        return False
    return commands.check(predicate)


async def is_owner(user_id):
    async def predicate(ctx):
        if ctx.guild.owner.id == user_id:
            return True

        return False
    return commands.check(predicate)
