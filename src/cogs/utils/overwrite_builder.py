import discord

from src.database import DatabaseMapperFactory as dmf


class OverwriteBuilder:

    def __init__(self, bot, guild, author=None):

        self.overwrites = {}

        self.bot = bot
        self.guild = guild
        self.author = author

    def add_bots(self):
        for member in self.guild.members:
            if member.bot:
                self.overwrites[member] = discord.PermissionOverwrite(
                        connect=True, view_channel=True, send_messages=True
                )

        return self

    def block_users(self):

        self.overwrites[self.guild.default_role] = discord.PermissionOverwrite(
                connect=False, view_channel=False, send_messages=False
        )

        return self

    def add_defaults(self):

        # Gives permission to the owner
        self.overwrites[self.author] = discord.PermissionOverwrite(
                connect=True, view_channel=True, send_messages=True
        )

        # Gives permission to the bot itself
        self.overwrites[self.bot.user] = discord.PermissionOverwrite(
            connect=True, view_channel=True, send_messages=True
        )

        return self

    def add_viewing_permission(self):
        roles = []
        for role in dmf.get_guild(self.guild.id).get_roles():
            if role.has_permission('view_rooms'):
                roles.append(self.guild.get_role(role.id))

        for role in roles:
            self.overwrites[role] = discord.PermissionOverwrite(
                connect=False, view_channel=True, send_messages=False
            )

        return self

    def add_join_permission(self):

        roles = []
        permission = dmf.get_permission()  # Get permission to join rooms
        for role in dmf.get_guild(self.guild.id).get_roles():
            if role.has_permission('enter_rooms'):
                roles.append(self.guild.get_role(role.id))

        for role in roles:
            if role not in self.overwrites.keys():
                self.overwrites[role] = discord.PermissionOverwrite(
                    connect=True, view_channel=True, send_messages=True
                )

        return self

    def get(self):
        self.add_bots()
        self.add_defaults()
        self.add_join_permission()
        self.add_viewing_permission()
        self.block_users()
        return self.overwrites

    @classmethod
    def get_user_permissions(cls, user, guild):

        overwrites = None

        for discord_role in user.roles:
            role = dmf.get_role(discord_role.id, guild.id).get()

            if not role:
                continue

            if not role.permissions:
                return discord.PermissionOverwrite(
                    connect=False, read_messages=False, send_messages=False
                )

            for permission in role.permissions:

                if permission.name == 'view_rooms':

                    overwrites = discord.PermissionOverwrite(
                        connect=False, read_messages=True, send_messages=True
                    )

                elif permission.name == 'manage_rooms' or permission.name == 'admin':

                    overwrites = discord.PermissionOverwrite(
                        connect=True, read_messages=True, send_messages=True
                    )

                    # No need to check further as the user has the maximum permit granted
                    break

            return overwrites
