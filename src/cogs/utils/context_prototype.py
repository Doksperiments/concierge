from src.database import DatabaseMapperFactory as dmf


class Context:

    def __init__(self, bot, member):
        self.bot = bot
        self.prefix = dmf.get_guild(member.guild.id).get().prefix
        self.guild = member.guild
        self.author = member

    def send(self, message):
        return
