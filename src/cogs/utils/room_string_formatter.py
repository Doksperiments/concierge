import re


def format_str(ctx, in_str: str):

    replacement = {
        "{username}": ctx.author.name,
        "{guild}": ctx.guild.name,
        "{prefix}": ctx.prefix
    }

    out = re.sub(r"{.*?}", lambda res: replacement[res.group()], in_str)
    return out

