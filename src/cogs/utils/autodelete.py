from aio_timers import Timer

pending_deletion_rooms = []


class AutoDelete(object):

    def __init__(self, bot, room, expire_time):
        self.room = room
        self.timer = Timer(expire_time, callback=self.delete, callback_args=(bot,), callback_async=True)
        self.deleting = False

    async def start(self):
        if not self.deleting:
            self.deleting = True
            pending_deletion_rooms.append(self)
            await self.timer.wait()

    async def delete(self, bot):

        if not self.deleting:
            await self.abort(self.room)
            return

        if not self.room.exists():
            pending_deletion_rooms.remove(self)
            return

        # Delete channels from discord
        voice = bot.get_channel(self.room.voice)
        text = bot.get_channel(self.room.text)

        await voice.delete()
        await text.delete()

        # Delete entry from the database and list
        self.room.remove()
        pending_deletion_rooms.remove(self)

        del self

    @classmethod
    async def abort(cls, room):
        for pending_room in pending_deletion_rooms:

            if pending_room.room.voice == room.voice:
                pending_room.timer.cancel()
                pending_deletion_rooms.remove(pending_room)
            else:
                return
