import discord
from discord.ext import commands

from src.cogs.utils.overwrite_builder import OverwriteBuilder
from src.cogs.utils.room_string_formatter import format_str
from src.database import DatabaseMapperFactory as dmf
from src.exceptions.database import DuplicateItemError
from src.resource import LanguageLoader


class Room(commands.Cog):

    options = ["name", "topic", "bitrate", "userlimit"]

    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    async def room(self, ctx):

        if ctx.invoked_subcommand is None:
            lang = dmf.get_guild(ctx.guild.id).get().lang
            await ctx.send(LanguageLoader.get_message("commandNotFound", lang))
            return

    @room.command(name='create')
    async def create(self, ctx):

        room = dmf.get_room(ctx.author.id, ctx.guild.id)

        if room.exists() is True:
            lang = dmf.get_guild(ctx.guild.id).get().lang
            await ctx.send(LanguageLoader.get_message("roomAlreadyExists", lang))
            return

        default_options = dmf.get_config(guild_id=ctx.guild.id).get()
        saved_room = {}  # FOR FUTURE IMPLEMENTATION

        guild = dmf.get_guild(ctx.guild.id).get()
        creator = discord.utils.get(ctx.guild.categories, id=guild.category)

        # If the category is not set use the guild object to create the channels
        if not creator:
            creator = ctx.guild

        log_channel = ctx.guild.get_channel(guild.log_channel)

        overwrites_new = OverwriteBuilder(self.bot, ctx.guild, ctx.author).get()

        # VOICE CHANNEL
        voice_channel = await creator.create_voice_channel(
            name=saved_room.get('name', format_str(ctx, default_options.name)),
            bitrate=saved_room.get('bitrate', default_options.bitrate),
            user_limit=saved_room.get('userlimit', default_options.userlimit),
            overwrites=overwrites_new
        )

        # TEXT CHANNEL
        text_channel = await creator.create_text_channel(
            name=saved_room.get('name', format_str(ctx, default_options.name)),
            topic=saved_room.get('topic', format_str(ctx, default_options.topic)),
            overwrites=overwrites_new
        )

        if voice_channel and text_channel:
            room.add()
            room.voice = voice_channel.id
            room.text = text_channel.id
        else:
            del room

            voice_channel.delete() if voice_channel else None
            text_channel.delete() if text_channel else None

        # Move the user if online
        try:
            await ctx.author.move_to(voice_channel)
        except discord.errors.HTTPException:
            # Don't do anything
            pass

        if log_channel:
            await log_channel.send(
                embed=LanguageLoader.get_embed("roomCreated", guild.lang, mention=ctx.author.mention)
            )

    @room.command(name='delete')
    async def _delete(self, ctx):

        room = dmf.get_room(ctx.author.id, ctx.guild.id).get()
        voice_channel = ctx.guild.get_channel(room.voice)
        text_channel = ctx.guild.get_channel(room.text)

        await voice_channel.delete() if voice_channel else None
        await text_channel.delete() if text_channel else None

        room.remove()

    @room.command(name='invite')
    async def _invite(self, ctx):

        invited_users = []

        room = dmf.get_room(ctx.author.id, ctx.guild.id).get()
        voice_channel = ctx.guild.get_channel(room.voice)
        text_channel = ctx.guild.get_channel(room.text)

        if ctx.message.mentions is None:
            # Ignore the request if no one was invited
            return

        for mention in ctx.message.mentions:

            # Ignore if the owner is tagged
            if mention == ctx.author:
                continue

            await voice_channel.set_permissions(
                mention, connect=True, view_channel=True
            )

            await text_channel.set_permissions(
                mention, view_channel=True, send_messages=True
            )

            invited_users.append(mention)

            user = dmf.get_user(mention.id)

            try:
                user.add()
            except DuplicateItemError:
                # Ignore if the user is already in the database
                pass

            room.invite_user(user)

        guild = dmf.get_guild(ctx.guild.id).get()
        log_channel = ctx.guild.get_channel(guild.log_channel)

        # Decide what message to send, based on the number of invited users
        if len(invited_users) == 1:

            if log_channel:
                await log_channel.send(
                    embed=LanguageLoader.get_embed(
                        "memberInvited", guild.lang, user=invited_users[0].mention, mention=ctx.author.mention
                    )
                )

            await ctx.send(LanguageLoader.get_message("memberInvited", guild.lang, user=invited_users[0].mention))
            return

        if log_channel:
            await log_channel.send(
                embed=LanguageLoader.get_embed(
                    "membersInvited", guild.lang, users=' '.join(
                        u.mention for u in invited_users
                    ),
                    mention=ctx.author.mention
                )
            )

        await ctx.send(
            LanguageLoader.get_message("membersInvited", guild.lang, users=' '.join(u.mention for u in invited_users))
        )

        return

    @room.command(name='kick')
    async def _kick(self, ctx):

        kicked_users = []

        room = dmf.get_room(ctx.author.id, ctx.guild.id).get()
        voice_channel = ctx.guild.get_channel(room.voice)
        text_channel = ctx.guild.get_channel(room.text)

        if ctx.message.mentions is None:
            # Ignore the request if no one was invited
            return

        for mention in ctx.message.mentions:

            # Ignore if the owner is tagged
            if mention == ctx.author:
                continue

            await voice_channel.set_permissions(mention, overwrite=None)
            await text_channel.set_permissions(mention, overwrite=None)

            await mention.move_to(None)
            kicked_users.append(mention)

            user = dmf.get_user(mention.id)

            try:
                user.add()
            except DuplicateItemError:
                # Ignore if the user is already in the database
                pass

            room.kick_user(user)

        guild = dmf.get_guild(ctx.guild.id).get()
        log_channel = ctx.guild.get_channel(guild.log_channel)

        # Decide what message to send, based on the number of kicked users
        if len(kicked_users) == 1:

            if log_channel:
                await log_channel.send(
                    embed=LanguageLoader.get_embed(
                        "memberKicked", guild.lang, user=kicked_users[0].mention, mention=ctx.author.mention
                    )
                )

            await ctx.send(LanguageLoader.get_message("memberKicked", guild.lang, user=kicked_users[0].mention))
            return

        if log_channel:
            await log_channel.send(
                embed=LanguageLoader.get_embed(
                    "membersKicked", guild.lang, users=' '.join(
                        u.mention for u in kicked_users
                    ),
                    mention=ctx.author.mention
                )
            )

        await ctx.send(
            LanguageLoader.get_message("membersKicked", guild.lang, users=' '.join(u.mention for u in kicked_users))
        )

        return

    @room.command(name='--help')
    async def room_help(self, ctx):
        guild = dmf.get_guild(ctx.guild.id).get()
        await ctx.send(embed=LanguageLoader.get_embed("helpRoom", guild.lang, prefix=guild.prefix))

    @room.group()
    async def edit(self, ctx):
        if ctx.invoked_subcommand is None:
            lang = dmf.get_guild(ctx.guild.id).get().lang
            await ctx.send(LanguageLoader.get_message("commandNotFound", lang))

    @edit.command(name='name')
    async def _name(self, ctx, *name):

        name = format_str(ctx, ' '.join(name))
        guild = dmf.get_guild(ctx.guild.id).get()

        if len(name) not in range(2, 101):
            await ctx.send(
                LanguageLoader.get_message("nameTooLong", guild.lang)
            )
            return

        log_channel = ctx.guild.get_channel(guild.log_channel) if guild.log_channel else None

        room = dmf.get_room(ctx.author.id, ctx.guild.id).get()
        voice = ctx.guild.get_channel(room.voice)
        text = ctx.guild.get_channel(room.text)

        if not voice or not text:
            await ctx.send(
                LanguageLoader.get_message("missingChannel", guild.lang)
            )
            return

        await voice.edit(name=name)
        await text.edit(name=name)

        if log_channel:
            await log_channel.send(
                embed=LanguageLoader.get_embed("roomNameEdited", guild.lang, mention=ctx.author.mention, new_name=name)
            )

        # Check if the log channel isn't the same as the invocation channel, to avoid double messages
        if log_channel != ctx.channel:
            await ctx.send(LanguageLoader.get_message("nameSet", guild.lang))
            return

    @edit.command(name='topic')
    async def _topic(self, ctx, *topic):

        topic = format_str(ctx, ' '.join(topic))
        guild = dmf.get_guild(ctx.guild.id).get()

        if len(topic) not in range(1, 1025):
            await ctx.send(LanguageLoader.get_message("topicTooLong", guild.lang))
            return

        log_channel = ctx.guild.get_channel(guild.log_channel) if guild.log_channel else None

        room = dmf.get_room(ctx.author.id, ctx.guild.id).get()
        text = ctx.guild.get_channel(room.text)

        if not text:
            await ctx.send(LanguageLoader.get_message("channelMissing", guild.lang))
            return

        await text.edit(topic=topic)

        await ctx.send(
            embed=LanguageLoader.get_embed("roomTopicEdited", guild.lang, mention=ctx.author.mention, new_topic=topic)
        )

        # Check if the log channel isn't the same as the invocation channel, to avoid double messages
        if log_channel != ctx.channel:
            await ctx.send(LanguageLoader.get_message("topicSet", guild.lang))
            return

    @edit.command(name='bitrate')
    async def _bitrate(self, ctx, bitrate):

        guild = dmf.get_guild(ctx.guild.id).get()
        # Bitrate is in Kbps
        # Not including boosted or nitro users
        if int(bitrate) not in range(8, 97):
            await ctx.send(LanguageLoader.get_message("bitrateNotInRange", guild.lang))
            return

        log_channel = ctx.guild.get_channel(guild.log_channel) if guild.log_channel else None

        room = dmf.get_room(ctx.author.id, ctx.guild.id).get()
        voice = ctx.guild.get_channel(room.voice)

        # Convert bitrate to bit per second
        bitrate = int(bitrate)*1000

        await voice.edit(bitrate=bitrate)

        await ctx.send(
            embed=LanguageLoader.get_embed(
                "roomBitrateEdited", guild.lang, mention=ctx.author.mention, new_bitrate=bitrate
            )
        )

        if log_channel != ctx.channel:
            await ctx.send(LanguageLoader.get_message("bitrateSet", guild.lang))
            return

    @edit.command(name='userlimit')
    async def _userlimit(self, ctx, userlimit):

        guild = dmf.get_guild(ctx.guild.id).get()

        if int(userlimit) not in range(100):
            await ctx.send(LanguageLoader.get_message("userLimitNotInRange", guild.lang))
            return

        log_channel = ctx.guild.get_channel(guild.log_channel) if guild.log_channel else None

        room = dmf.get_room(ctx.author.id, ctx.guild.id).get()
        voice = ctx.guild.get_channel(room.voice)

        await voice.edit(user_limit=int(userlimit))

        await ctx.send(
            embed=LanguageLoader.get_embed(
                "roomUserLimitEdited", guild.lang, mention=ctx.author.mention, new_user_limit=userlimit
            )
        )

        if log_channel != ctx.channel:
            await ctx.send(LanguageLoader.get_message("userLimitSet", guild.lang))
            return

    @edit.command(name='--help')
    async def edit_help(self, ctx):
        guild = dmf.get_guild(ctx.guild.id).get()
        await ctx.send(embed=LanguageLoader.get_embed("helpRoomEdit", guild.lang, prefix=guild.prefix))


def setup(bot):
    bot.add_cog(Room(bot))
