import discord
from discord.ext import commands

from lang import lang_handler as lang


class Help(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.bot.remove_command('help')

    @commands.command()
    async def help(self, ctx):
        if ctx.invoked_subcommand is None:
            await ctx.send(embed=lang.get_embed('help', prefix=ctx.prefix))


def setup(bot):
    bot.add_cog(Help(bot))
