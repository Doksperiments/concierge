import re

import discord
from discord.ext import commands

from src.database import DatabaseMapperFactory as dmf
from src.resource import LanguageLoader


def has_permission(permission):
    async def predicate(ctx):

        if ctx.guild.owner.id == ctx.author.id:
            return True

        roles = [role for role in dmf.get_guild(ctx.guild.id).get_roles() if role.has_permission(permission)]
        if not roles:
            return False

        for role in roles:
            if role.id in (r.id for r in ctx.author.roles):
                return True

        lang = dmf.get_guild(ctx.guild.id).get().lang
        await ctx.send(LanguageLoader.get_message("insufficientPermission", lang))
        return False

    return commands.check(predicate)


class Settings(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.group()
    async def guild(self, ctx):

        if ctx.invoked_subcommand is None:
            lang = dmf.get_guild(ctx.guild.id).get().lang
            await ctx.send(LanguageLoader.get_message("commandNotFound", lang))
            return

    @guild.command(name='--help')
    async def guild_help(self, ctx):
        guild = dmf.get_guild(ctx.guild.id).get()
        await ctx.send(embed=LanguageLoader.get_embed("helpGuild", guild.lang, prefix=guild.prefix))

    @has_permission("manage_bot")
    @guild.group()
    async def set(self, ctx):

        if ctx.invoked_subcommand is None:
            lang = dmf.get_guild(ctx.guild.id).get().lang
            await ctx.send(LanguageLoader.get_message("commandNotFound", lang))
            return

    @set.command(name='category')
    async def _category(self, ctx, *category):

        guild = dmf.get_guild(ctx.guild.id).get()

        if not category:
            current_category = guild.category
            current_category = discord.utils.get(ctx.guild.categories, id=current_category)

            if not current_category:
                await ctx.send(LanguageLoader.get_message("categoryNotSet", guild.lang))
                return

            await ctx.send(
                embed=LanguageLoader.get_embed("currentCategory", guild.lang, actual_category=current_category)
            )
            return

        if re.fullmatch(r"[0-9]{17,20}", category[0]):

            new_category = discord.utils.get(ctx.guild.categories, id=int(category[0]))

            if new_category:
                guild.category = new_category.id
                await ctx.send(embed=LanguageLoader.get_embed("categorySet", guild.lang, new_category=new_category.name))
                return

        if category[0] == 'null':
            guild.category = 0
            await ctx.send(LanguageLoader.get_message("categoryRemoved", guild.lang))
            return

        new_category = discord.utils.get(ctx.guild.categories, name=' '.join(category))

        if new_category:
            guild.category = new_category.id
            await ctx.send(embed=LanguageLoader.get_embed("categorySet", guild.lang, new_category=new_category.name))

        else:
            # if the category still can't be found, then raise an error and tell the user
            await ctx.send(LanguageLoader.get_message("categoryNotFound", guild.lang))
            raise ValueError("Couldn't find a valid category")

    @set.command(name='lobby')
    async def _lobby(self, ctx, *lobby):

        guild = dmf.get_guild(ctx.guild.id).get()

        if not lobby:
            current_lobby = guild.lobby
            current_lobby = ctx.guild.get_channel(current_lobby).name

            if not current_lobby:
                await ctx.send(LanguageLoader.get_message("lobbyNotSet", guild.lang))
                return

            await ctx.send(
                embed=LanguageLoader.get_embed("currentLobby", guild.lang, actual_category=current_lobby)
            )
            return

        if re.fullmatch(r"[0-9]{17,20}", lobby[0]):

            new_lobby = ctx.guild.get_channel(int(lobby[0]))

            if new_lobby:
                guild.lobby = new_lobby.id
                await ctx.send(embed=LanguageLoader.get_embed("lobbySet", guild.lang, new_lobby=new_lobby.name))
                return

        if lobby[0] == 'null':
            guild.lobby = 0
            await ctx.send(LanguageLoader.get_message("lobbyRemoved", guild.lang))
            return

        new_lobby = discord.utils.get(ctx.guild.channels, name=' '.join(lobby))

        if new_lobby:
            guild.lobby = new_lobby.id
            await ctx.send(embed=LanguageLoader.get_embed("lobbySet", guild.lang, new_lobby=new_lobby.name))

        else:
            # if the lobby still can't be found, then raise an error and tell the user
            await ctx.send(LanguageLoader.get_message("lobbyNotFound", guild.lang))
            raise ValueError("Couldn't find a valid lobby")

    @set.command(name='log')
    async def _log(self, ctx, *log_channel):

        guild = dmf.get_guild(ctx.guild.id).get()
        if not log_channel:
            current_log_channel = guild.log_channel
            current_log_channel = ctx.guild.get_channel(current_log_channel).name

            if not current_log_channel:
                await ctx.send(LanguageLoader.get_message("logChannelNotSet", guild.lang))
                return

            await ctx.send(
                embed=LanguageLoader.get_embed("currentLogChannel", guild.lang, actual_category=current_log_channel)
            )
            return

        if re.fullmatch(r"[0-9]{17,20}", log_channel[0]):

            new_log_channel = ctx.guild.get_channel(int(log_channel[0]))

            if new_log_channel:
                guild.log_channel = new_log_channel.id
                await ctx.send(
                    embed=LanguageLoader.get_embed("logChannelSet", guild.lang, new_log_channel=new_log_channel.name)
                )
                return

        if log_channel[0] == 'null':
            guild.log_channel = 0
            await ctx.send(LanguageLoader.get_message("logChannelRemoved", guild.lang))
            return

        new_log_channel = discord.utils.get(ctx.guild.channels, name='-'.join(log_channel))

        if new_log_channel:
            guild.log_channel = new_log_channel.id
            await ctx.send(
                embed=LanguageLoader.get_embed("logChannelSet", guild.lang, new_log_channel=new_log_channel.name)
            )

        else:
            # if the category still can't be found, then raise an error and tell the user
            await ctx.send(LanguageLoader.get_message("logNotFound", guild.lang))
            raise ValueError("Couldn't find a valid log_channel")

    @set.command(name='prefix')
    async def _prefix(self, ctx, prefix):

        # Remove every whitespace possible
        prefix = ''.join(prefix.split())
        guild = dmf.get_guild(ctx.guild.id).get()

        if len(prefix) > 3:
            await ctx.send(LanguageLoader.get_message("prefixTooLong", guild.lang))
            raise ValueError("Prefix too long (max 3 characters)")

        guild.prefix = prefix
        await ctx.send(embed=LanguageLoader.get_embed("prefixSet", guild.lang, prefix=prefix))

    @set.command(name='expiretime')
    async def _expire_time(self, ctx, expire_time):

        expire_time = int(expire_time)
        guild = dmf.get_guild(ctx.guild.id).get()

        if expire_time not in range(0, 60):
            await ctx.send(LanguageLoader.get_message("expireTimeNotInRange", guild.lang))
            raise ValueError("Expire time not in range (0-60)")

        guild.expire_time = expire_time
        await ctx.send(embed=LanguageLoader.get_embed("expireTimeSet", guild.lang, new_value=expire_time))

    @set.command(name='--help')
    async def set_help(self, ctx):

        guild = dmf.get_guild(ctx.guild.id).get()

        await ctx.send(
            embed=LanguageLoader.get_embed("helpGuild", guild.lang, prefix=guild.prefix)
        )

    @set.group()
    async def default(self, ctx):

        if ctx.invoked_subcommand is None:
            lang = dmf.get_guild(ctx.guild.id).get().lang
            await ctx.send(LanguageLoader.get_message("commandNotFound", lang))
            return

    @default.command(name='name')
    async def _name(self, ctx, *name):

        name = ' '.join(name)
        lang = dmf.get_guild(ctx.guild.id).get().lang

        if len(name) not in range(2, 101):
            await ctx.send(LanguageLoader.get_message("nameTooLong", lang))
            return

        dmf.get_config(guild_id=ctx.guild.id).get().name = name

        await ctx.send(LanguageLoader.get_message("defaultNameSet", lang))

    @default.command(name='topic')
    async def _topic(self, ctx, *topic):

        topic = ' '.join(topic)
        lang = dmf.get_guild(ctx.guild.id).get().lang

        if len(topic) not in range(1, 1025):
            await ctx.send(LanguageLoader.get_message("topicTooLong", lang))
            return

        dmf.get_config(guild_id=ctx.guild.id).get().topic = topic

        await ctx.send(LanguageLoader.get_message("defaultTopicSet", lang))

    @default.command(name='bitrate')
    async def _bitrate(self, ctx, bitrate):

        lang = dmf.get_guild(ctx.guild.id).get().lang

        # Bitrate is in Kbps
        # Not including boosted or nitro users
        if int(bitrate) not in range(8, 97):
            await ctx.send(LanguageLoader.get_message("bitrateNotInRange", lang))
            return

        bitrate = int(bitrate)*1000

        dmf.get_config(guild_id=ctx.guild.id).get().bitrate = bitrate

        await ctx.send(LanguageLoader.get_message("defaultBitrateSet", lang))

    @default.command(name='userlimit')
    async def _userlimit(self, ctx, userlimit):

        lang = dmf.get_guild(ctx.guild.id).get().lang

        if int(userlimit) not in range(100):
            await ctx.send(LanguageLoader.get_message("userLimitNotInRange", lang))
            return

        dmf.get_config(guild_id=ctx.guild.id).get().userlimit = int(userlimit)

        await ctx.send(LanguageLoader.get_message("defaultUserLimitSet", lang))

    @default.command(name='--help')
    async def default_help(self, ctx):
        guild = dmf.get_guild(ctx.guild.id).get()
        await ctx.send(embed=LanguageLoader.get_embed("helpDefaultRoomEdit", guild.lang, prefix=guild.prefix))


def setup(bot):
    bot.add_cog(Settings(bot))
