import discord
from discord.ext import commands

from src.database import DatabaseMapperFactory as dmf
from src.exceptions.database import DuplicateItemError
from src.resource import LanguageLoader


class Permission(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.is_owner()
    @commands.group()
    async def permission(self, ctx):

        if ctx.invoked_subcommand is None:
            lang = dmf.get_guild(ctx.guild.id).get().lang
            await ctx.send(LanguageLoader.get_message("commandNotFound", lang))
            return

    @permission.command(name='add')
    async def _add(self, ctx, role_id, permission_name):

        discord_role = ctx.guild.get_role(int(role_id))
        lang = dmf.get_guild(ctx.guild.id).get().lang

        if not discord_role:
            await ctx.send(LanguageLoader.get_message("roleIdNotFound", lang))
            raise ValueError("Role id is wrong")

        if permission_name != "manage_bot":
            await ctx.send(LanguageLoader.get_message("invalidPermission", lang))
            raise ValueError("Permission name not recognized")

        role = dmf.get_role(role_id, ctx.guild.id)
        try:
            role.add()
        except DuplicateItemError:
            role.get()

        permission = dmf.get_permission(permission_name).get()
        role.add_permission(permission)

        await ctx.send(
            embed=LanguageLoader.get_embed("permissionAdded", lang, permission=permission_name, role=discord_role.name)
        )

    @permission.command(name='remove')
    async def _remove(self, ctx, role_id, permission_name):

        discord_role = ctx.guild.get_role(int(role_id))
        lang = dmf.get_guild(ctx.guild.id).get().lang

        if not discord_role:
            raise ValueError("Role id is wrong")

        if permission_name != "manage_bot":
            raise ValueError("Permission name not recognized")
        role = dmf.get_role(role_id, ctx.guild.id)

        try:
            role.add()
        except DuplicateItemError:
            role.get()

        permission = dmf.get_permission(permission_name).get()
        role.remove_permission(permission)

        await ctx.send(
            embed=LanguageLoader.get_embed(
                "permissionRemoved", lang, permission=permission_name, role=discord_role.name
            )
        )

    @permission.command(name='--help')
    async def permission_help(self, ctx):
        guild = dmf.get_guild(ctx.guild.id).get()
        await ctx.send(embed=LanguageLoader.get_embed("helpPermission", guild.lang, prefix=guild.prefix))


def setup(bot):
    bot.add_cog(Permission(bot))
