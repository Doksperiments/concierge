from discord.ext import commands

from src.database import DatabaseMapperFactory as dmf
from src.exceptions.database import DuplicateItemError

from src.cogs.utils.autodelete import AutoDelete
from src.cogs.utils.context_prototype import Context


class EventHandler(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

        # Try adding the global permissions and the 0 user and guild
        permissions = ['view_rooms', 'enter_rooms', 'manage_permissions', 'manage_bot', 'admin']
        for permission in permissions:
            try:
                dmf.get_permission(permission).add()
            except DuplicateItemError:
                continue

        try:
            dmf.get_user(0).add()
            dmf.get_guild(0).add()
        except DuplicateItemError:
            pass

    @commands.Cog.listener()
    async def on_message(self, message):

        if message.author.bot:
            return

        try:
            dmf.get_user(message.author.id).add()
        except DuplicateItemError:
            # Ignore the exception as the user is already in the database
            pass

        await self.bot.process_commands(message)

    @commands.Cog.listener()
    async def on_guild_join(self, guild):

        try:
            dmf.get_guild(guild.id).add()
        except DuplicateItemError:
            # Ignore the exception as the guild is already in the database
            pass

        dmf.get_config(guild_id=guild.id).add()

        # Give roles with admin power the ability to control the bot
        for role in guild.roles[::-1]:
            if role.permissions.administrator and role.is_bot_managed() is False:
                admin = dmf.get_role(role.id, guild.id)
                try:
                    admin.add()
                except DuplicateItemError:
                    admin.get()

                admin_perm = dmf.get_permission('admin').get()
                admin.add_permission(admin_perm)

    @commands.Cog.listener()
    async def on_guild_leave(self, guild):

        dmf.get_config(guild_id=guild.id).remove()
        dmf.get_guild(guild.id).remove()

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):

        if before.channel == after.channel:
            return

        guild = dmf.get_guild(member.guild.id).get()

        # Lobby
        if after.channel:
            if guild.lobby is not None and after.channel.id == guild.lobby:
                user_room = dmf.get_room(member.id, member.guild.id)

                # If the room already exists just move the user
                if user_room.exists():
                    await member.move_to(member.guild.get_channel(user_room.get().voice))
                else:
                    await self.bot.get_cog('Room').create(Context(self.bot, member))

                return

        # Autodelete

        rooms = dmf.get_room().get_all(guild.id)

        for room in rooms:

            if before.channel is not None:

                # Check that the guild has autodelete active, otherwise skip execution
                if not guild.expire_time:
                    return

                if before.channel.id == room.voice and room.exists():

                    # Count members in the room
                    member_list = 0
                    for member in before.channel.members:
                        if not member.bot:
                            member_list += 1

                    if member_list == 0:
                        # If the room doesn't exist anymore (deleted already manually) stop execution
                        if not room.exists():
                            return

                        # Start the autodelete process
                        ad = AutoDelete(self.bot, room, guild.expire_time)
                        await ad.start()

            if after.channel is not None:
                if after.channel.id == room.voice:
                    # Stop the autodeletion in case it was running
                    await AutoDelete.abort(room)


def setup(bot):
    bot.add_cog(EventHandler(bot))
