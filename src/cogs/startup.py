import discord
from discord.ext import commands

from src.database import DatabaseMapperFactory as dmf


class Startup(commands.Cog):

    def __init__(self, bot):
        self.bot: discord.ext.commands.Bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print("Startup ready")
        for guild in self.bot.guilds:
            rooms = dmf.get_room().get_all(guild.id)
            for room in rooms:
                # Check if the voice and text channels exist and if they are empty
                pass
