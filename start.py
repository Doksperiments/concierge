import asyncio
import logging

from src.main import Bot
from src.resource import ConfigLoader


async def start():

    bot = Bot()
    await bot.start(token)

if __name__ == '__main__':

    logging.basicConfig(level=logging.WARNING)
    start_loop = asyncio.get_event_loop()
    token = ConfigLoader.get_token()

    try:
        start_loop.run_until_complete(start())
    except ConnectionError as err:
        print("Start process failed, check log file")
        # LOG: CRITICAL: Error starting
